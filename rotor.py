import os
from pathlib import Path
import random
import time
import argparse


def rotate(bg_paths: str, interval_mins: int):
    hits = []
    for bg_path in bg_paths:
        bg_path = Path(bg_path)
        hits += list(bg_path.rglob("*.jpg")) + list(bg_path.rglob("*.jpeg")) + list(bg_path.rglob("*.png"))
    print(f"Found {len(hits)} backgrounds. Rotation interval set to {interval_mins} minutes.")
    os.system("gsettings set org.gnome.desktop.background picture-options 'scaled'")
    while True:
        bg_img_path = random.choice(hits)
        os.system('gsettings set org.gnome.desktop.background picture-uri "'+str(bg_img_path)+'"')
        time.sleep(interval_mins * 60)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-bg", "--bg_paths", 
                        nargs='+',
                        help="path to background images (retrieves recursively all jpg & png images).",
                        required=True,
                        type=str)
    parser.add_argument("-mins", "--interval_mins",
                        help="Number of minutes to randomly rotate to next bg img.",
                        default=10,
                        type=int)
    args = parser.parse_args()
    rotate(args.bg_paths, args.interval_mins)
